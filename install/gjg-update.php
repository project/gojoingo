<?php

// This file should be run from your root Drupal directory.
// Please BACK UP YOUR DATABASE before running this script.

include_once "includes/bootstrap.inc";
include_once 'includes/common.inc';

// 2006-03-15: 
// external_link added to og core module as 'website'
// need to move data if it exists
if (!field_exists('website', 'og')) {
  db_query('ALTER TABLE {og} ADD COLUMN website VARCHAR(255) NULL');
}
if (field_exists('external_link', 'og')) {
  $result = db_query("SELECT nid, external_link FROM {og} WHERE  external_link != ''");
  while ($group = db_fetch_object($result)) {
    db_queryd("UPDATE {og} SET website = '%s' WHERE nid = %d", $group->external_link, $group->nid);
  }
  db_queryd("ALTER TABLE {og} DROP COLUMN external_link");
}

/**
 * Support function; determine if given field exists in given table.
 *
 * @param $field
 *   Name of field
 * @param $table
 *   Name of table
 * @return boolean
 *   TRUE if field was found, FALSE if it was not.
 */
function field_exists($field, $table) {
  // Start by assuming we didn't find the field
  $found = FALSE;

  // Retrieve a list of all fields
  $result = db_query("DESC {$table}");

  // Go through each field to see if it matches
  while ($object = db_fetch_object($result)) {
    if ($object->Field == $field) {
      $found = TRUE;
      break;
    }
  }

  return $found;
}