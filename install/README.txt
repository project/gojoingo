

This folder contains files related to installing/upgrading GoJoinGo.

===================
= gjgmasterscript =
===================

DESCRIPTION
-----------
This is a bash script to perform an installation of Drupal 4.6 with all
required modules for GoJoinGo.

USAGE
-----
1. Place the gjgmasterscript file into the folder where you would like 
   to install Drupal.
2. Enter the following from your shell prompt:
   chmod u+x gjgmasterscript
3. Execute the script by typing the following:
   ./gjgmasterscript

REQUIREMENTS
------------
- bash
- cvs
- wget
- tar
- patch
- mysql

CHANGELOG
---------
2006-Mar-15:
- Fixed creation date
- Made Drupal checkout go to a folder called 'gojoingo' rather than 'drupal'
- Retrieve phptemplate via cvs checkout rather than wget
- Modules now checkout into directories in the modules folder, rather than
  in contributions/modules/module_name
- Added checkout for logintoboggan
- Added checkout for urlfilter
- Re-ordered checkouts in alphabetical order
- Changed comment above spam module
- Made extracting spam module archive into one operation
- Removed filetree_linker subroutine
- Added location module's zipcodes.us.mysql
- og_forum now an og contrib module
- Fixed errors when no DB prefixes
- Fixed prefixing logic
- Pointed all GJG-related mysql statements to the install.mysql file
- Included patch for prefix.sh for UPDATE and DROP TABLE statements

==================
= gjg-update.php =
==================

DESCRIPTION
-----------
A script to handle database updates of GoJoinGo.

USAGE
-----
1. BACK UP YOUR DATABASE!!!
2. Copy the gjg-update.php file to the root of your Drupal installation
3. Execute the script by going to http://www.example.com/gjg-update.php

=================
= install.mysql =
=================

DESCRIPTION
-----------
All of the various GJG-specific .mysql files in one easy-to-access place. 
You may also opt to pick and choose which ones to install by going into each
module folder individually.

CHANGELOG
---------
2006-Mar-15:
- Removed gjg_user table
  (see: http://drupal.org/node/48607)
- Removed statement to add 'external_link' to og module 
  (see: http://drupal.org/node/53903)
