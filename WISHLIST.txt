
===================
 GOJOINGO WISHLIST
===================

What follows is my analysis of the code in the GoJoinGo collection of 
modules/patches and what areas I would like to see improved. If you'd like
to help with any of this, please don't hesitate to contact me!
http://drupal.org/user/24967/contact

==========
 THE GOAL
==========
I am striving to make GoJoinGo a collection of small, focused modules that 
each implement one piece of the 'social networking super-site' puzzle which 
can either be used together, or indepdently. The functionality within 
GoJoinGo should eventually allow any Drupal site to compete with the 
following types of sites:

- Evite
- Meetup
- MySpace
- Friendster
- LinkedIn
- Upcoming.org
- del.icio.us

However, it will take some work to get all the way there. ;)

===============
 GLOBAL ISSUES
===============
There are three main things I think would really help this package:

1. No more reliance on core patches: Patching core files scares the hell
   out of people, and with good reason. In 4.7, almost all of the 
   functionality we have patches for is already in core, and what remains
   we should be able to use hook_form_alter for (for the most part). I 
   don't anticipate a solution for this in 4.6.

2. Installer difficulty: This is probably also not feasible for the 4.6 
   version, but for 4.7 there is enough installer functionality in core
   that we could make an 'installer' for GoJoinGo, or at least make the 
   various DB changes *much* easier to implement. An interim solution
   is a "Configuration Checklist" for people to run down and make sure
   they have enabled all the options and configured all the settings.

3. Reduce inter-dependence on modules: There are a few places where 
   there is inter-dependence on modules that needs to be stripped out and
   refactored; most notably the events and invitations systems. I am hoping
   to have this completed for the 4.6 version so that 4.7 is just a simple
   port of Forms API and other 4.7-related things.

========
 EVENTS
========
Ideally, I would like to see a 'global' vision for events functionality for
all of Drupal, and have GoJoinGo use this. Something like the following:

- Calendar: Ties date/time to any node type; view calendar of nodes. Currently
  satisfied by event.module.
- Attendance: Allows sign-ups to any node type, and tracks attendance. 
  Partially satisfied by signup.module. 
- Invitations: Handles inviting users (including anonymous users) to any
  type of node. GJG's Invite API module gets us sort of there.
- Group events: A module to restrict calendar view to events from groups.

=============
 INVITATIONS
=============
Invitations are critical to the type of 'viral' marketing the web depends on.
GJG implements the following types of invitations:

- Group invitations
- Event invitations
- Site invitations

Ideally, I would like Invite API to turn into an actual Invite API, which
can be used by other modules (without hacking). There might be possibilities
here with the Mime Mail module, which can act on any user_mail call.

===============
 MISC. CLEANUP
===============
- Move the 'my home' portions out of front.module, replace front.module with 
  the actual full-blown Front Page module. This is both more flexible and also
  eliminates a naming conflict with CivicSpace sites.

==========
 MORE...
==========

To be fleshed out when I get another few spare minutes. ;)
