This patch incorporates two changes:

1. Includes a performance improvement for node_access queries. See
   issue: http://drupal.org/node/36429
2. Forces users to go through the OG module to create new content...
   Make sure to enable the group details block!
