This patch adds the AJAX autocomplete functions to common.inc, as they are in 4.7.

The files:

- autocomplete.js
- drupal.js

need to be moved to the 'misc' directory.

- advanced.css

needs to go in your theme's directory.