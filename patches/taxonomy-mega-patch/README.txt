This patch adds the following features to 4.6 taxonomy:
- Free tagging support
  (present in 4.7 -- see http://drupal.org/node/20936)
- Autocomplete support (AJAX)
  (present in 4.7 -- see http://drupal.org/node/22519)
