This patch adds:
- Autocomplete 
  (present in 4.7 -- see http://drupal.org/node/22519)
- Password reset functionality 
  (present in 4.7 -- see http://drupal.org/node/18719)
  
Please note that the password reset functionality requires a 
'login' field to be added to the user table. see the .mysql file.