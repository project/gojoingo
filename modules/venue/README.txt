
venue.module

DESCRIPTION:
A node module that allows the creation of a venue in which an event 
will take place

REQUIRES:
Intended for use with event.module.

INSTALLATION:
1. Execute the SQL in the .mysql file.

2. Place the directory into your Drupal modules/ directory.

3. Enable the module in administer >> modules.
