
userstag.module:

DESCRIPTION:
Provides users with the opportunity to 'tag' themselves with taxonomy terms.

REQUIRES:
- Taxonomy

INSTALLATION:
1. Execute the SQL in the .mysql file.

2. Place the directory into your Drupal modules/ directory.

3. Enable the module in administer >> modules.

TODO:
- Comments
