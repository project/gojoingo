
gsearch.module

DESCRIPTION:
This module allows searching for specific node types and users by location.

REQUIRES:
- location.module
- search.module

INSTALLATION:
1. Place the directory into your Drupal modules/ directory.

2. Enable the module in administer >> modules.
