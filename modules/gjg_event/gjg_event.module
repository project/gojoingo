<?php


/**
 * @file
 * Enables the creation of events that can be added to the system.
 */

/**
 * Implementation of hook_help().
 */
function gjg_event_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Enables the creation of events.');
    case 'node/add#gjg_event':
      return t('Create events.');
  }
}

/**
 * Implementation of hook_perm().
 */
function gjg_event_perm() {
  return array('create events', 'edit own events');
}

/**
 * Implementation of hook_menu().
 */
function gjg_event_menu($may_cache) {
  global $user;
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'node/add/gjg_event', 'title' => t('event'),
      'access' => gjg_event_access('create', NULL));
    $items[] = array('path' => 'rsvp', 'title' => t('RSVP'), 'callback' => 'gjg_event_rsvp_form', 'access' => $user->uid, 'type' => MENU_CALLBACK);
/*
    $items[] = array('path' => 'rsvp_page', 'title' => t('RSVPd page'), 'callback' => 'gjg_event_rsvp_page', 'access' => $user->uid, 'type' => MENU_CALLBACK);
*/
    $items[] = array('path' => 'gjg_event/autocomplete/venue', 'title' => t('venue autocomplete'), 'callback' => 'gjg_event_autocomplete_venue', 'access' => $user->uid, 'type' => MENU_CALLBACK);
    $items[] = array('path' => 'event_invite', 'title' => t('Create invite'), 'callback' => 'gjg_event_create_invite', 'access' => $user->uid, 'type' => MENU_CALLBACK);
  }
  else {
    if (db_num_rows(db_query_range('SELECT n.nid FROM {node} n INNER JOIN {event} e ON n.nid = e.nid AND event_end > %d WHERE uid = %d AND type = "gjg_event"', time(), $user->uid, 0, 1))) {
      $items[] = array('path' => 'my/events/hosting', 'title' => t('Hosting'),
        'access' => $user->uid, 'type' => MENU_DEFAULT_LOCAL_TASK);
      $items[] = array('path' => 'my/events/rsvp', 'title' => t("RSVP'd"),
        'access' => $user->uid, 'type' => MENU_LOCAL_TASK);
    }
    if (arg(0) == 'og' && is_numeric(arg(1)) && arg(2) == 'events') {
      $items[] = array('path' => 'og/'. arg(1) .'/events', 'title' => t('Events'),
        'access' => node_access(arg(1), 'view'), 'callback' => 'gjg_event_group',  'type' => MENU_CALLBACK);
    }
  }

  return $items;
}

/**
 * Implementation of hook_node_name().
 */
function gjg_event_node_name($node) {
  return t('event');
}

/**
 * Implementation of hook_access().
 */
function gjg_event_access($op, $node) {
  global $user;

  if ($op == 'create') {
    return user_access('create events');
  }

  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own events') && ($user->uid == $node->uid)) {
      return TRUE;
    }
  }
}

function gjg_event_link($type, $node = NULL) {
  global $user;
  if ($type == 'node' && $node->type == 'gjg_event' && $user->uid != $node->uid) {
    $links = array(l(t('RSVP'), "rsvp/$node->nid"));
/*
    if (node_access('update', $node)) {
      $links[] = l(t('RSVPd page'), "rsvp_page/$node->nid");
    }
*/
    return $links;
  }
}

/**
 * Implementation of hook_form().
 */
function gjg_event_form(&$node) {
  if (function_exists('taxonomy_node_form')) {
    $output .= implode('', taxonomy_node_form('gjg_event', $node));
  }
  $output .= form_textarea(t('Long description'), 'body', $node->body, 60, 20, '', NULL, TRUE);
  $output .= filter_form('format', $node->format);
  $output .= form_textfield(t('Short description'), 'description', $node->description, 40, 60);
  $output .= form_autocomplete(t('Name of the venue (type the first few letters)'), 'venue', $node->venue, 60, 100, 'gjg_event/autocomplete/venue', t('The name of the venue. To create a new venue click %here', array('%here' => l('here', 'node/add/venue'))));
  $output .= form_textfield(t('Cost'), 'cost', $node->cost, 10, 8);
  $output .= form_textfield(t('Max event capacity'), 'capacity', $node->capacity, 5, 8, t('Set to 0 for unlimited capacity'));
  $options = array(t('No one'), t('Group members and invitees can bring their friends'), t('Everyone'));
  $output .= form_radios(t('Who else can attend?'), 'attend', $node->attend, $options, t('Group members and those I invited can always attend.'));
  return $output;
}

function gjg_event_insert($node) {
  db_query("INSERT INTO {gjg_event} (nid, description, venue_id, cost, attend, capacity) VALUES(%d, '%s', %d, '%s', %d, %d)", $node->nid, $node->description, $node->venue_id, $node->cost, $node->attend, $node->capacity);
  gjg_event_invite_group($node->nid);
}

function gjg_event_update($node) {
  db_query("UPDATE {gjg_event} SET description = '%s', venue_id = %d, cost = '%s', attend = %d, capacity = %d WHERE nid = %d", $node->description, $node->venue_id, $node->cost, $node->attend, $node->capacity, $node->nid);
}

function gjg_event_delete($node) {
  db_query('DELETE FROM {gjg_event} WHERE nid = %d', $node->nid);
}

function gjg_event_validate(&$node) {
  global $user;
  if (!$node->nid && empty($_POST)) {
    return;
  }
  if (!empty($node->cost) && (!is_numeric($node->cost) || $node->cost < 0)) {
    form_set_error('cost', t('Cost must be a number'));
  }
  if ($node->capacity && (!is_numeric($node->capacity) || $node->capacity < 0)) {
    form_set_error('capacity', t('Capacity must be a number if filled'));
  }
  $node->venue_id = db_result(db_query("SELECT nid FROM {node} WHERE title = '%s'", $node->venue));
  if (!$node->venue_id) {
    form_set_error('venue', t('Non-existent venue'));
  }

}

/**
 * Invite group members to an event.
 */
function gjg_event_invite_group($nid) {
  $node = node_load(array('nid' => $nid));
  if ($node->nid) {
    if (isset($node->og_groups[0])) {
      $group = node_load(array('nid' => $node->og_groups[0]));
      $result = db_query(og_list_users_sql(), $group->nid);
      $subject = t('Invitation to event %event', array('%event' => $node->title));
      $body = t("Hi.\n\n I invite to the event %event. You can RSVP by clicking the link below.\n\n", array('%event' => $node->title));
      $body .= t("The event is about %description\n\n", array('%description' => $node->description));
      $callbacks[] = array('accept callback' => 'gjg_event_rsvp', 'type' => 'event', 'callback arguments' => array($node->nid));
      $timeout = 86400;
      while ($member = db_fetch_object($result)) {
        invite_api_send($member->uid, $subject, $body, $callbacks, $timeout);
        watchdog ('gjg_event', "mail: $user->mail, subj: $subj, body: $body");
      }
    }
  }
}

function gjg_event_create_invite($nid) {
  $node = node_load(array('nid' => $nid));
  if ($node->nid) {
    $node->invite = $node->og_public;
    if (!$node->invite) {
      $group = node_load(array('nid' => $node->og_groups[0]));
      if ($group->og_selective == OG_OPEN || node_access('update', $group)) {
        $node->invite = TRUE;
      }
    }
    if ($node->invite) {
      if (!empty($_POST['edit']['mails'])) {
        $max = variable_get('og_email_max', 10);
        $emails = explode(',', $_POST['edit']['mails']);
        if (count($emails) > $max) {
          form_set_error('mails', t('You may not specify more than %max email addresses.', array('%max' => $max)));
          return;
        }
        foreach ($emails as $email) {
          if (!valid_email_address(trim($email))) {
            $bad[] = $email;
          }
        }
        if ($bad) {
          form_set_error('mails', t('invalid email address: '). implode(' ', $bad));
          return;
        }
        /** GJG: This is triggering even when inviting to event! **/
        if (isset($node->og_groups[0])) {
          $group = node_load(array('nid' => $node->og_groups[0]));
          $group_name = $group->title;
          $subj = t('Invitation to join the group "%group" at %site and to event %event', array('%group' => $group->title, '%site' => variable_get('site_name', 'drupal'), '%event' => $node->title));
          $body = t("Hi.\n\n I invite to the event %event. As this event belongs to  '%group' please join this group as well by clicking the link below.\n\n", array('%event' => $node->title, '%group' => $group->title));
          $body .= t("The group is about %description\n\n", array('%description' => $group->og_description));
          $body .= t("The event is about %description\n\n", array('%description' => $node->description));
        }
        else {
          $subj = t('Invitation to event %event', array('%group' => $group->title, '%site' => variable_get('site_name', 'drupal'), '%event' => $node->title));
          $body = t("Hi.\n\n I invite to the event %event. You can RSVP by clicking the link below.\n\n", array('%event' => $node->title));
          $body .= t("The event is about %description\n\n", array('%description' => $node->description));
        }
        $body .= t("Subscribe: %accept-link\n\nDeny: %deny-link");
        $body .= $edit['pmessage'] ? "\n\n ----------------------------\n". $edit['pmessage']. "\n--------------------------------" : '';
        global $user;
        foreach ($emails as $mail) {
          $mail = trim($mail);
          $callbacks = array();
          if (!db_num_rows(db_query("SELECT uid FROM {users} WHERE uid != %d AND LOWER(mail) = LOWER('%s')", $user->uid, $mail))) {
            $callbacks[] = array('accept callback' => 'user_invite_form', 'type' => 'user', 'callback arguments' => array());
            $timeout = 86400;
          }
          else {
            $timeout = 0;
          }
          if (isset($node->og_groups[0])) {
            $callbacks[] = array('accept callback' => 'og_invite_accept', 'type' => 'og', 'callback arguments' => array($group->nid));
          }
          $callbacks[] = array('accept callback' => 'gjg_event_rsvp', 'type' => 'event', 'callback arguments' => array($node->nid));
          watchdog ('gjg_event', "mail: $mail, subj: $subj, body: $body");
          invite_api_send($mail, $subj, $body, $callbacks, $timeout);
        }
        drupal_set_message('Invites sent');
        drupal_goto("node/$node->nid");
      }


//    $output .= form_autocomplete(t('Users to be invited'), 'names', $node->names, 60, 100, 'gjg_event/autocomplete/name', t('A comma-separated list of user names (Example: funny, bungie jumping, "Company, Inc.").'));
      $output .= form_textfield(t('Email addresses to be invited'), 'mails', $_POST['edit']['mails'], 60, 100, t('A comma-separated list of email addresses.'));
      $output .= form_submit(t('Send invite'));
      print theme('page', form($output));
    }
  }
  else {
    drupal_not_found();
  }
}



function gjg_event_load($node) {
  $additions = db_fetch_object(db_query('SELECT e.*, v.title as venue FROM {gjg_event} e INNER JOIN {node} v ON e.venue_id = v.nid WHERE e.nid = %d', $node->nid));
  return $additions;
}

function _gjg_event_autocomplete($string = '') {
  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  // This regexp allows the following types of user input:
  // this, "somecmpany, llc", "and ""this"" w,o.rks", foo bar
  $regexp = '%(?:^|,\ *)("(?>[^"]*)(?>""[^"]* )*"|(?: [^",]*))%x';
  preg_match_all($regexp, $string, $matches);
  $array = $matches[1];

  // Fetch last tag
  $last_string = trim(array_pop($array));
  if ($last_string != '') {
    $result = db_query_range('SELECT name FROM {users} WHERE LOWER(name) LIKE LOWER("%s%%"', $string, 0, 10);

    $prefix = count($array) ? implode(', ', $array) .', ' : '';

    $matches = array();
    while ($tag = db_fetch_object($result)) {
      $n = $tag->name;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (preg_match('/,/', $tag->name) || preg_match('/"/', $tag->name)) {
        $n = '"'. preg_replace('/"/', '""', $tag->name) .'"';
      }
      $matches[$prefix . $n] = check_plain($tag->name);
    }
    print drupal_implode_autocomplete($matches);
    exit();
  }
}

function gjg_event_autocomplete_venue($string = '') {
  $matches = array();
  $result = db_query_range("SELECT title FROM {node} WHERE type = 'venue' AND LOWER(title) LIKE LOWER('%s%%')", $string, 0, 10);
  while ($node = db_fetch_object($result)) {
    $matches[$node->title] = check_plain($node->title);
  }
  print drupal_implode_autocomplete($matches);
  exit();
}

function gjg_event_invite_page($uid1, $uid2, $nid) {
  return gjg_event_rsvp_form($nid);
}

function gjg_event_rsvp_form($nid) {
  global $user;

  $node = node_load(array('nid' => $nid));
  if (!$node->nid || !node_access('view', $node)) {
    drupal_access_denied();
    exit;
  }

  if (isset($_POST['op']) && $_POST['op'] == t('Submit')) {
    gjg_event_rsvp_save($_POST['edit']);
  }
  $attend = db_result(db_query('SELECT attend FROM {attend} WHERE uid = %d AND nid = %d', $user->uid, $nid));
  $output .= "<h3>$node->title</h3>";
  if (isset($attend)) {
    $output .= '<p>'. t("You already RSVP'd, now you can modify it") .'</p>';
  }
  if ($node->capacity) {
    $output .= t('Free capacity: %d', array('%d' => $node->capacity - db_result(db_query('SELECT SUM(attend) FROM {attend} WHERE nid = %d AND attend != 0 GROUP BY nid', $nid))));
  }
  $form = form_radios(t('Please RSVP'), 'rsvp', $attend > 0, array(t("Can't make it"), t('Will attend')));
  if ($node->attend) {
    $form .= form_textfield(t('Number of others'), 'attend', $attend ? $attend - 1 : 0, 3, 4, t('How many people will you bring besides yourself?'));
  }
  $form .= form_submit(t('Submit'));
  $output .= form($form);
  print theme('page', $output);
}

function gjg_event_rsvp_save($edit) {
  global $user;
  if (!$edit['rsvp'] && $edit['attend']) {
    form_set_error('attend', t('Sorry, no cigar. (You should not see this error message ever.)'));
  }
  $nid = arg(1);
  $sum = db_result(db_query('SELECT SUM(attend) FROM {attend} WHERE nid = %d AND attend != 0 AND uid != %d GROUP BY nid', $nid, $user->uid));
  $node = node_load(array('nid' => $nid));
  $attend = $edit['rsvp'] + $edit['attend'];
  if ($node->capacity && ($sum + $attend > $node->capacity)) {
    form_set_error('rsvp', t('Unfortunately this event does not have enough capacity'));
  }
  if (!form_get_errors()) {
    db_query('REPLACE INTO {attend} (nid, uid, attend) VALUES (%d, %d, %d)', $nid, $user->uid, $attend);
    drupal_set_message(t('You registered %n people to the event.', array('%n' => $attend)));
  }
}



function gjg_event_view(&$node, $teaser = FALSE) {
  if (!$teaser) {
    if (node_access('update', $node) && $node->event_end > time()) {
      $node->body = '<p>'. l(t('Invite people'), "event_invite/$node->nid") .'</p>'. $node->body;

    }
    $node->body .= gjg_event_rsvp_page($node->nid, TRUE);

    $node->body .= '<p><b>Venue:</b> '. $node->venue . '</p>';
    $venue = node_load(array('nid' => $node->venue_id));
    $node->body .= theme('location',$venue->location);
  }
}

function gjg_event_rsvp_page($nid, $return = FALSE) {
  $node = node_load(array('nid' => $nid));
  if (!$node->nid || !node_access('view', $node)) {
    drupal_access_denied();
    exit;
  }

  if (!$return) {
    $output .= "<h3>$node->title</h3>";
  }
  $result = db_query('SELECT name, u.uid, attend - 1 AS attend FROM {attend} a INNER JOIN {users} u ON a.uid = u.uid WHERE nid = %d AND attend > 0 ORDER BY name ASC', $nid);
  $rows = array();
  while ($rsvp = db_fetch_object($result)) {
    $rows[] = array(l($rsvp->name, "user/$rsvp->uid"), array('data' => $rsvp->attend, 'align' => 'center'));
  }
  if (!empty($rows)) {
    $output .= theme('table', array(t('User name'), t('How many people will (s)he bring along')), $rows);
  }
  else {
    $output = t('<p>No one RSVPd yet</p>');
  }
  if ($return) {
    return $output;
  }
  print theme('page', $output);
}


function gjg_event_get_events($gid) {
  $sql_pre = "SELECT DISTINCT(n.nid), n.title, n.body, n.format, e.event_start, g.cost, g.description
  FROM {node} n
  INNER JOIN {event} e ON n.nid = e.nid
  INNER JOIN {gjg_event} g ON n.nid = g.nid
  INNER JOIN {node_access} og_na ON n.nid = og_na.nid
  WHERE og_na.gid = %d AND og_na.realm = 'og_group' AND n.status=1 AND n.type = 'gjg_event' AND event_start ";
  $sql_post = ' UNIX_TIMESTAMP() ORDER BY n.sticky DESC';
  $ops = array('>', '<=');
  $num = array(2,4);
  $headers = array(t('Upcoming Events'), t('Past Events'));
  $nodes = array();
  foreach ($ops as $key => $op) {
    $result = pager_query("$sql_pre$op$sql_post", $num[$key], $key, NULL, $gid);
    while ($node = db_fetch_object($result)) {
      $nodes[] = $node;
    }
  }
  return $nodes;
}

function gjg_event_group() {
  $gid = arg(1);
  if ($title = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $gid))) {
    drupal_set_title(t('%title: events', array('%title' => $title)));
  }
  else {
    drupal_not_found();
    exit;
  }
  $nodes = gjg_event_get_events($gid);
  $rows = array();
  foreach ($nodes as $node) {
    $rows[] = array(
      l($node->title, "node/$node->nid"),
      format_date($n->event_start, 'custom', 'Y-m-d'),
      format_date($n->event_start, 'custom', 'h:ia'),
      $node->cost,
      truncate_utf8(check_plain($node->description), 100));
  }
  $header = array('Event name', 'Date', 'Start', 'Cost', 'Description');
  //$output .= "<h3>$headers[$key]</h3>";
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', $num[$key], $key);
  print theme('page', $output);
}
