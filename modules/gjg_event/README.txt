
gjg_event.module:

DESCRIPTION:
Allows people to register for events, assigns events to venues, rsvp for
events and invite people to events.

REQUIRES:
- event.module
- venue.module

INSTALLATION:
1. Execute the SQL in the .mysql file.

2. Place the directory into your Drupal modules/ directory.

3. Enable the module in administer >> modules.

TODO:
- Clean up comments/documentation
- Implement hook_help