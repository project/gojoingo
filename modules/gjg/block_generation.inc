<?php



//////////////////////////////////////////////////////////////////////////////////

/**
 *
 * Retrieve the default php block snippets.
 *
 */
function gjg_block_snippets($block_name) {
  switch($block_name) {
   	case 'group_profile':
	  $code_block = '
<?php
	/* Edit the following variables if you\'d like to change the text for this block */
	$description = \'Description\';
	$organizer = \'Organizer\';
	$location = \'Location\';
	$members  = \'Members\';   
	
	/* Below is PHP code, only edit if you feel comfortable with PHP and the Drupal API.  */
	global $user;
	if ($gid = $_SESSION[\'og_last\']->nid) {
	$node = node_load(array(\'nid\' => $gid));
	$result = db_query(og_list_users_sql(0), $node->nid);
	$cntall = db_num_rows($result);
	if (!empty($node->files) && is_array($node->files)) {
	  foreach ($node->files as $file) {
		if ($file->filename == \'thumbnail\') {
		  $node->og_picture = file_create_path($file->filepath);
		}
	  }
	}
	$picture = theme(\'image\', $node->og_picture);
	$default = theme(\'image\', \'files/images/user_image_not_found.png\');
	$content = $picture ? $picture : $default;
	$content .= \'<div class="group-group">\';
	$content .= \'<div class="group-title">\'. t($description) .\'</div>\';
	$content .= \'<div class="group-item">\'. check_plain($node->og_description) .\'</div>\';
	$content .= \'</div>\';
	$content .= \'<div class="group-group">\';
	$content .= \'<div class="group-title">\'. t($organizer) .\'</div>\';
	$content .= \'<div class="group-item">\'. check_plain($node->firstname .\' \'. $group->lastname) .\'</div>\';
	$content .= \'</div>\';
	$content .= \'<div class="group-group">\';
	$content .= \'<div class="group-title">\'. t($location) .\'</div>\';
	$content .= \'<div class="group-item">\'. check_plain($node->address) .\'</div>\';
	$content .= \'</div>\';
	$content .= \'<div class="group-group">\';
	$content .= \'<div class="group-title">\'. t($members) .\'</div>\';
	$content .= \'<div class="group-item">\'. format_plural($cntall-$cntpending, \'1 member\', \'%count members\') .\'</div>\';
	$content .= \'</div>\';    
	print $content;
	}
?> '; //end of the code_block;
      return $code_block; //return the above php snippet.
	
	case 'group_actions':
	  $code_block = '
<?php
  /* Edit the following variables if you would like to change the text for this block */
  $my_subscription = "my subscription";
  $invite_friend = "invite friend";
  $approval = "Your subscription request awaits approval.";
  $delete_request = "delete request";
  $request_subscription = "request subscription";
  $subscribe_txt = "subscribe";
  $group_manager = "group manager: ";
  $age = "age: ";
  
  /* Below is PHP code, only edit if you feel comfortable with PHP and the Drupal API.  */
  global $user;
  if ($node = og_get_group_context()) {
    $result = db_query(og_list_users_sql(0), $node->nid);
    $cntall = db_num_rows($result);
    $cntpending = 0;
    while ($row = db_fetch_object($result)) {
      if ($row->is_active == 0) {
        $cntpending++;
      }
      if ($row->uid == $user->uid) {
        if ($row->is_active) {
          $subscription = \'active\';
        }
        else {
          $subscription = \'requested\';
        }
      }
    }
    $output = "<div id=\"og-description\">". check_plain($node->og_description). "</div>";
    if ($subscription == \'active\') {
      $links = module_invoke_all(\'og_create_links\', $node);
      $links[] = l($my_subscription, "og/manage/$node->nid");
      if ($node->og_selective == OG_OPEN || db_result(db_query("SELECT grant_update FROM {node_access} WHERE realm = \'og_uid\' AND gid = %d AND nid = %d", $user->uid, $node->nid))) {
        $links[] = l($invite_friend, \'og/invite/\'. $node->nid, array(), drupal_get_destination());
      }
      $txt =  \'<div class="members">\'. format_plural($cntall-$cntpending, \'1 subscriber\', \'%count subscribers\') .\'</div>\';
      $links[] = $txt;
      $extra = \'<div class="organizer">\'. $group_manager .\'</div>\'. (variable_get(\'og_member_pics\',1) ? theme(\'user_picture\',$node) : \'\') . format_name($node);
    }
    elseif ($subscription == \'requested\') {
      $links[] = $approval;
      $links[] = l($delete_request, "og/unsubscribe/$node->nid", array(), \'destination=og\');
    }
    elseif ($user->uid && $node->og_selective < OG_INVITE_ONLY) {
      if ($node->og_selective == OG_MODERATED) {
        $txt = $request_subscription;
      }
      elseif ($node->og_selective == OG_OPEN) {
        $txt = $subscribe;
      }
      $links[] = l($txt, "og/subscribe/$node->nid", array(), "destination=node/$node->nid");
    }
    $output .= theme(\'item_list\', $links);
    $extra .= $node->images ? image_display($node, \'thumbnail\') : \'\';
    $output .= \'<div class="block_details_extra">\'. $extra .\'</div>\';
    print $output;
  }
?>'; //end of php snippet
		return $code_block; //return the above php snippet
	case 'my_friends':
	    $code_block = '
<?php
 /* Edit the following variables if you would like to change the text for this block */
  $my_friends = "My friends";
  $no_friends = "You do not have any friends yet.";
   
   /* Below is PHP code, only edit if you feel comfortable with PHP and the Drupal API.  */
  // Show My Friends block only for authenticated users
  global $user; 
  if (!$uid = $user->uid) {
	print "";
  }
  $result = db_query_range("
	SELECT u.uid
	FROM {gjg_user_user} f
	INNER JOIN {users} u ON ((f.uid1 = %d AND u.uid = f.uid2) OR (f.uid2 = %d AND u.uid = f.uid1))
	WHERE f.status = \'f\'
	ORDER BY f.status DESC, u.name", $uid, $uid, 0, 4);
  $accounts = array();
  while ($account = db_fetch_object($result)) {
	$accounts[] = user_load(array(\'uid\' => $account->uid));
  }

  // Check if friends exist
  if (count($accounts) > 0) {
	print theme(\'list_users\', $accounts);
  }
  else {
	print t($no_friends);
  }
?>'; //end php snippet
          return $code_block; //return the above php snippet
	case 'personal_content':
	  $code_block = '
<?php
	/**  You can edit this block\'s HTML, just preserve the 
	 *  dynamic PHP used to obtain the user ID: $GLOBALS[\'user\']->uid 
	 *  and also the ?q= style links (unless you know you have clean URLs turned on.)
	 **/
    print \'
	<div class="menu">
	<ul>
	<li><a href="?q=my/groups">My Groups</a></li>
	<li><a href="?q=my/events">My Events</a></li>
	<li><a href="?q=my/friends">My Friends</a></li>
	<li><a href="?q=my/bookmarks">My Bookmarks</a></li>
	<li><a href="?q=tracker/\'. $GLOBALS[\'user\']->uid .\'">My Posts</a></li>
	<li><a href="?q=my/stats">My Stats</a></li>
	</ul>
	</div>\';
?>'; //end of php snippet
		return $code_block; //return above snippet
	case 'user_block':
	  $code_block = '
<?php
  /* Edit the following variables if you\'d like to change the text for this block */
  $inbox = \'My Inbox\';
  $messages = \'View Messages\';
  $invite = \'Invite a Friend \';
  $my_account = \'My Account \';
  $view_profile = \'View Profile \';
  $update_account = \'Update Account\';
  $logout = \'Log out \';
  
  /* Below is PHP code, only edit if you feel comfortable with PHP and the Drupal API.  */
  global $user;
  $account = $user;
  $unread = (int)_privatemsg_get_new_messages($user->uid);   
  // Hide for anonymous users
	if (!$user->uid) {
		return;
	}
	  $picture = theme(\'user_picture\', $account);
	  $default = theme(\'image\', \'files/images/user_image_not_found.png\');
	  $content .= $picture ? $picture : l($default, "user/$account->uid/edit/personal", array(), NULL, NULL, FALSE, TRUE);
	  $title = t($inbox);
	  $items = array(
		l(t($messages), \'privatemsg\') . \'<div class="count">[\'. $unread .\']</div>\',
		l(t($invite), \'create/invite\')
	  );
	 $content .= theme(\'user_block_item_group\', $title, $items);
	 $title = t($my_account);
	 $items = array(
	   l(t($view_profile), "user/$account->uid"),
	   l(t($update_account), "user/$account->uid/edit"),
	   l(t($logout), \'logout\')
	 );
	 $content .= theme(\'user_block_item_group\', $title, $items);
	 print $content;
?>'; //end of php snippet
       return $code_block;  //return above php snippet
	case 'recommendations':
	  $code_block = '
<?php
  /* Edit the following variables if you\'d like to change the text for this block */
  $groups = \'Groups in your Area\';
  $events = \'Events in your area\';
  $people = \'People in your area\';
  $popular = \'Popular Groups\';
  $new = \'New Groups\';
  
  /* Below is PHP code, only edit if you feel comfortable with PHP and the Drupal API.  */
  global $user;
	$items = array(l(t($groups), \'gsearch/og\'),
				   l(t($events), \'gsearch/gjg_event\'),
				   l(t($people), \'gsearch/user\'),
				   l(t($popular), \'og/popular\'),
				   l(t($new), \'og/new\'));
   $output = theme(\'gjg_menu\',$items);
   print $output;
?>'; //end of php snippet
       return $code_block; //return the above code snippet
  }

}

?>