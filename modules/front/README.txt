
front.module:

DESCRIPTION:
This module displays a customized front page for a user showing their group 
membership and events, or a customizable message for anonymous users.

REQUIRES:
- event
- gjg_event
- og

INSTALLATION:
1. Place the directory into your Drupal modules/ directory.

2. Enable the module in administer >> modules.

3. Go to administer >> settings >> front and enter the default message that guests should see.