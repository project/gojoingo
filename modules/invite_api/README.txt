
invite_api/user_invite:

DESCRIPTION:
invite_api establishes a framework for sending invitations to users. 
user_invite sends invitations from user to user for friend and buddy 
requests.

REQUIRES:
N/A

INSTALLATION:
1. Execute the SQL in the .mysql file.

2. Place the directory into your Drupal modules/ directory.

3. Enable the modules in administer >> modules.

TODO:
- Clean up comments
- Fill in some missing hooks
- Move e-mails to settings page
- Documentation