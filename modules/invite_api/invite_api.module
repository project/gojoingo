<?php
/*
1. $uid1 invites $uid2 for

invite($uid, $data)

$data is an array or an array of arrays.
One such array contains the following key-value pairs:
'message' => $invitation_message required
'subject' => $subject
'callback' => 'on_accept_call_this',
'type' => user, friend, organizer, group member, event, register confirmation
'timeout' => when it will expire

  //db_query("INSERT INTO {gjg_user_user} (uid1, uid2, status, timestamp) VALUES (%d, %d, 'i', %d)", $user->uid, $account->uid, $time);
  if ($gid) {
    $node = node_load(array('nid' => $nid));
    if ($node->og_selective == OG_OPEN || db_result(db_query("SELECT grant_update FROM {node_access} WHERE realm = 'og_uid' AND gid = %d AND nid = %d", $user->uid, $node->nid))) {
      db_query('INSERT INTO {og_uid} (nid, uid) VALUES (%d, %d)', $gid, $account->uid);
      db_query("INSERT INTO {node_access} (nid, gid, realm, grant_view) VALUES (%d, %d, 'og_uid', 1)", $gid, $account->uid);
    }
  }

*/

/**
 * Implementation of hook_menu().
 */
function invite_api_menu($may_cache) {
  global $user;
  $items = array();
  if (!$may_cache) {
    $items[] = array('path' => 'invite', 'access' => TRUE, 'type' => MENU_CALLBACK, 'title' => '', 'callback' => 'invite_api_page');
    $items[] = array('path' => 'invite_api_move', 'access' => $user->uid, 'type' => MENU_CALLBACK, 'title' => '', 'callback' => 'invite_api_move');
  }
  return $items;
}


/**
 * Implementation of hook_settings().
 */
function invite_api_settings() {
  $group = form_textfield(t('Maximum number of addresses per invite'), 'invite_api_max', variable_get('invite_api_max', 10), 3, 5, t('This value will be used to determine the maximum number of people to whom users may send invites.'));
  $group .= form_textfield(t('E-mail address separator'), 'invite_api_separator', variable_get('invite_api_separator', ','), 3, 5, t('This is the separator used between multiple e-mail addresses.'));
  return form_group(t('Invite API settings'), $group);
}

/**
  @param
    $recipient  user to be invited
  @param
    $subject  the subject of the mail to be sent
  @param
    $message  the body of the mail to be sent
  @param
    $callbacks  array of arrays containing 'type', 'accept callback',  'deny callback' = '', 'callback arguments' = array()
  @param
    $timeout  timeout = 630720000
  @return
    errorstring on fault
*/
function invite_api_send($recipient, $subject, $message, $callbacks, $timeout = 630720000) {
  global $user, $base_url;

  foreach ($callbacks as $callback) {
    if (!isset($callback['callback arguments'])) {
      $callback['callback arguments'] = array();
    }
  }
  $time = time() + $timeout;
  $iid_key = user_password();
  $iid = db_next_id('{invite}_iid');
  if (!is_numeric($recipient)) {
    if (!$uid = db_result(db_query("SELECT uid FROM {users} WHERE LOWER(mail) = LOWER('%s')", $recipient))) {
      if ($error = _invite_api_validate_mail($recipient)) {
        return $error;
      }
      $account = _invite_api_external($recipient, $subject, $message, $iid, $iid_key);
      $mailed = 1;
      $recipient = $account->uid;
    }
    else {
      $recipient = $uid;
    }
  }
  if (!isset($account)) {
    $account = user_load(array('uid' => $recipient));
  }
  $link = $base_url . url('/invite/'. $recipient .'/'. $iid .'/'. $iid_key);
  $message = str_replace('%accept-link', $link .'/1', $message);
  $message = str_replace('%deny-link', $link .'/0', $message);
  db_query("INSERT INTO {invite} (iid, uid1, uid2, callbacks, timeout, active, iid_key) VALUES (%d, %d, %d, '%s', %d, 1, '%s')", $iid, $user->uid, $recipient, serialize($callbacks), $time, $iid_key);
  if (!$mailed) {
    invite_api_mail($recipient, $subject, $message);
  }
}

function invite_api_mail($uid, $subject, $message) {
  global $sent_mail;
  $account = user_load(array('uid' => $uid));
  _privatemsg_edit(array('recipient' => $account->name, 'subject' => $subject, 'message' => $message));
  if (!$sent_mail)  {
    $from = variable_get('site_mail', ini_get('sendmail_from'));
    $headers = "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from";
    watchdog('invite', "to: $account->mail message: $message");
    user_mail($account->mail, $subject, $message, $headers);
  }
}

/**
 * Validates the e-mail address of a recipient to ensure that it is:
 * - Formatted properly
 * - Does not already exist
 * - Is not denied acccess
 *
 * @param $mail
 *   E-mail addresss
 * @return
 *   Error message if a problem was encountered, else nothing.
 */
function _invite_api_validate_mail($mail) {
  if ($error = user_validate_mail($mail)) {
    return $error;
  }
  else if (db_result(db_query("SELECT uid FROM {users} WHERE LOWER(mail) = LOWER('%s')", $mail))) {
    return t('The e-mail address %email is already taken.', array('%email' => theme('placeholder', $mail)));
  }
  else if (user_deny('mail', $mail)) {
    return t('The e-mail address %email has been denied access.', array('%email' => theme('placeholder', $mail)));
  }
}

/**
 * Process an invite to a user who has not signed up for the site yet.
 *
 * Creates a new user record for them, and then sends the invitation.
 *
 * @param $mail
 *   E-mail address
 * @param $subject
 *   Subject of mail
 * @param $message
 *   Mail message
 * @param $iid
 *   Invitation ID (for invite table)
 * @param $iid_key
 *   Random string to verify invitation
 * @return
 *   Recipient's new user record, or else nothing if there was an error.
 */
function _invite_api_external($mail, $subject, $message, $iid, $iid_key) {
  global $user, $base_url;
  $pass = user_password();
  $account = user_save('', array('name' => $mail, 'pass' => $pass, 'init' => $mail, 'roles' => array(_user_authenticated_id()), 'login' => 0, 'status' => 0, 'mail' => $mail));
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  $headers = "From: $from\nReply-to: $from\nX-Mailer: Drupal\nReturn-path: $from\nErrors-to: $from";
  $link = $base_url . url('/invite/'. $account->uid .'/'.  $iid .'/'. $iid_key);
  $message = str_replace('%accept-link', $link .'/1', $message);
  $message = str_replace('%deny-link', $link .'/0', $message);
  if (user_mail($account->mail, $subject, $message, $headers)) {
    watchdog('user', t('%user invited %mail', array('%user' => $user->name, '%mail' => $account->mail)));
  }
  return $account;
}

/**
 * Removes expired invitations from the queue.
 */
function invite_api_cron() {
  $time = time();
  $result = db_query('SELECT * FROM {invite} WHERE timeout < %d AND timeout > 0', $time);
  while ($invite = db_fetch_object($result)) {
    $callbacks = unserialize($invite->callbacks);
    $types = array();
    foreach ($callbacks as $callback) {
      if (!empty($callback['deny callback'])) {
        $arguments = array_merge(array($invite->uid1, $invite->uid2), $callback['callback arguments']);
        call_user_func($callback['deny callback'], $arguments);
      }
      $types[] = $callback['type'];
    }
    if ($invite->uid1 != $invite->uid2) {
      $account = user_load(array('uid' => $invite->uid2));
      invite_api_mail($invite->uid1, t('Invitation result'), t('Invitation to %mail of type %type has expired.', array('%mail' => $account->mail, '%type' => implode(',', $types))));
    }
  }
  db_query('UPDATE {invite} SET timeout = 0 WHERE timeout < %d', $time);
}

function invite_api_page($uid, $iid, $iid_key, $accept) {
  global $user;

  // Check if invitation exists that matches the given key
  if ($invite = db_fetch_object(db_query("SELECT * FROM {invite} WHERE iid = %d AND iid_key ='%s' AND active = 1", $iid, $iid_key))) {
    // Retrieve callback functions from invitation
    $callbacks = unserialize($invite->callbacks);
    // Remove the first callback
    $callback = array_shift($callbacks);
    // Is this a site invitation?
    if ($callback['accept callback'] == 'user_invite_form') {
      // If a user is already logged in, prompt them to continue
      if ($user->uid) {
        drupal_set_message(t('You are logged in as %name. If you are not %name then please use the register form below. If you want to continue as %name please click %here', array('%name' => $user->name, '%here' => l(t('here'), "invite_api_move/$user->uid/$iid/$iid_key/$accept"))));
      }
    }
    elseif ($uid != $user->uid) {

      // The wrong user is attempting to validate this invitation

      //watchdog logger.
      watchdog('invite', 'User was denied access');
      drupal_access_denied();
    }

    // If we get here, the invitation is valid
    
    //Watchdog logger
    watchdog('invite', 'On accept, '. $callback['accept callback'] .' will be called.  On deny, '. $callback['deny callback'] .'.  Accept variable was set to '. $accept .'.');

    // Determine whether to use the accept or deny callback
    $callback_func = $accept ? $callback['accept callback'] : $callback['deny callback'];
    
    // Does callback function exist?
    if (function_exists($callback_func)) {
      // Build arguments array (uid1, uid2, <whatever>)
      $arguments = array_merge(array($invite->uid1, $invite->uid2), $callback['callback arguments']);
      // Call the function
      $return = call_user_func_array($callback_func, $arguments);
      if ($return == array($invite->uid1, $invite->uid2)) {
        
        invite_api_next();
      }
      else {
        return $return;
      }
    }
  }
  else {
    // User tried to enter an invalid invitation URL
    drupal_access_denied();
  }
}

/**
 * Obtain number of invitations 'in progress'; that is, they have been
 * sent but not acted upon and have not timed out.
 *
 * @param $uid
 *   Recipient's user ID
 * @param $type
 *   Type of invitation
 * @return
 *   Number of in progress invitations
 */
function invite_api_in_progress($uid, $type) {
  global $user;

  return db_num_rows(db_query("SELECT * FROM {invite} WHERE callbacks LIKE '%%s:4:\"type\";s:%d:\"%s\";%%' AND uid1 = %d AND uid2 = %d AND timeout > %d", strlen($type), $type, $user->uid, $uid, time()));
}

function invite_api_move($uid, $iid, $iid_key, $accept) {
  global $user;
  if ($invite = db_fetch_object(db_query("SELECT * FROM {invite} WHERE iid = %d AND iid_key ='%s' AND timeout > %d", $iid, $iid_key, time()))) {
    $callbacks = unserialize($invite->callbacks);
    $callback = array_shift($callbacks);
    if ($callback['accept callback'] == 'user_invite_form') {
      db_query('UPDATE {invite} SET uid2 = %d WHERE iid = %d', $user->uid, $iid);
      invite_api_next();
    }
  }
}

function invite_api_next($path = '') {
  $invite = db_fetch_object(db_query('SELECT * FROM {invite} WHERE iid = %d ', arg(2)));
  if (!$invite) {
    drupal_set_message('There was a problem processing this invite.', 'error');
    return;
  }
  $callbacks = unserialize($invite->callbacks);
  $callback = array_shift($callbacks);
  db_query("UPDATE {invite} SET callbacks = '%s' WHERE iid = %d", serialize($callbacks), $invite->iid);
  if (empty($callbacks)) {
    if ($invite->uid1 != $invite->uid2) {
      $account = user_load(array('uid' => $invite->uid2));
      if (arg(4)) {
        invite_api_mail($invite->uid1, t('Invitation result'), t('Invitation to %name of type %type is accepted.', array('%name' => $account->name, '%type' => $callback['type'])));
      }
      else {
        invite_api_mail($invite->uid1, t('Invitation result'), t('Invitation(s) to %name of type %type is rejected.', array('%name' => $account->name, '%type' => $callback['type'])));
      }
    }
    drupal_goto();
  }
  else {
    drupal_goto(str_replace('invite_api_move', 'invite', $_GET['q']));
  }
}


function invite_api_form($description = '', $request = '', $max = 10) {
  $edit = $_POST['edit'];
  if ($max) {
    $form .= form_textfield(t('E-mail addresses'), 'mails', $edit['mails'], 90, 250, $description ? $description : t('Enter up to %max e-mail addresses. Separate multiple addresses by commas. Each will receive an invitation message from you.', array('%max' => $max)), NULL, TRUE);
  }
  $form .= form_textarea(t('Personal message'), 'pmessage', $edit['pmessage'], 90, 5, t('Optional. Enter a message which will become part of the invitation e-mail'));
  $form .= form_submit($submit ? $submit : t('Send invitation'));

  return form($form);
}

/**
 * Replaces the variables in e-mail subjects and messages.
 *
 * @param $message
 *   The original message
 * @param $recipient_uid
 *   The user id of the person recieiving the message, if applicable
 * @return
 *   Formatted message
 */
function _invite_api_parse_variables($message, $recipient_uid = 0) {
  global $user;

  if ($recipient_uid != 0) {
    $recipient = user_load(array('uid' => $recipient_uid));
  }

  $message = str_replace('%site-name', variable_get('site_name', $_SERVER['HTTP_HOST']), $message);
  $message = str_replace('%user', $user->name, $message);
  if (isset($recipient)) {
    $message = str_replace('%recipient', $recipient->name, $message);
  }

  return $message;
}

?>