
file.module: 

DESCRIPTION:
This is just a simple node module that, when upload-enabled, allows users to 
upload files.

REQUIRES:
Doesn't *require* upload.module, but is pretty useless without it. :)

INSTALLATION:
1. Place the directory into your Drupal modules/ directory.

2. Enable the module in administer >> modules.

3. Enable attachments from administer >> content >> configure >> types >> file.
