This directory will contain modules for the site GoJoinGo -- useful for 
sites which want to utilize the power of organic groups for a social
networking website.

The basic setup is as follows:

Drupal 4.6.x

Core modules:
- blog
- comment
- forum
- help
- menu
- node
- page
- path
- search
- statistics
- story
- taxonomy
- tracker
- upload

Contrib modules for core gjg functionality:
- event (for event scheduling)
- image (for group images)
- location (for tying groups, users, etc. to locations)
- node_type_filter (for filtering node listings by type)
- og (for groups)
- og_forum (available in og/contrib)
- privatemsg (for private messages)

Other contrib modules:
- feedback (for the feedback form)
- logintoboggan (for customized login system)
- spam (for reporting posts functionality, spam protection)
  NOTE: GoJoinGo uses the 'report as spam' available from version 2.0.13-pre1
  version of the spam module, NOT the 1.0 version in Drupal CVS.
  Obtain it from: http://www.kerneltrap.org/jeremy/drupal/spam/
- urlfilter (for automatically changing invitation links in private messages
  to clickable URLs)

GoJoinGo modules:
- file
- front
- gjg
- gjg_event
- group_block
- gsearch
- invite_api
- user_block
- user_invite
- userstag
- venue

Patches:
- common-inc-autocomplete:
  This enabled autocomplete AJAX functionality from 4.7 for Drupal 4.6.
- location-register:
  This simple patch adds the location form into the user registration form.
- node-node_access:
  This patch restricts access to node/add
- og-mega-patch:
  This patch makes numerous changes to the organic groups module
- privatemsg-email:
  This patch adds code to email private messages back to users.
- taxononomy-mega-patch:
  This patch adds in free tagging, autocomplete, and sql_rewrite support from 
  Drupal 4.7
- user-mega-patch:
  This adds request password functionality from Drupal 4.7

Run the install.mysql file to add all table definitions.